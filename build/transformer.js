"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Function that gets the correct formatted design tokens for each "flavor"
 * available with in the design token json. These are grouped by theme ("day", "night", "vodafone", "ziggo", "converged")
 *
 * First key is type (color, gradient, etc.)
 * Second key is theme (day, night, vodafone, ziggo, converged)
 *
 * Example input:
 * {
 *   "color": {
 *     "day": {
 *       "background": {
 *         "background": {
 *           "category": "color",
 *           "exportKey": "color",
 *           "comment": "{gray50}",
 *           "value": "rgba(240, 241, 241, 1)",
 *           "type": "color"
 *         }
 *     },
 *     "night": {
 *         "background.surface": {
 *           "category": "color",
 *           "exportKey": "color",
 *           "comment": "{white}",
 *           "value": "rgba(255, 255, 255, 1)",
 *           "type": "color"
 *         }
 *       }
 *     }
 *   }
 * }
 *
 * Example output:
 * {
 *   "day": {
 *     "color": {
 *       "background": "rgba(240, 241, 241, 1)",
 *     }
 *   },
 *   "night": {
 *     "color": {
 *       "backgroundSurface": "rgba(255, 255, 255, 1)"
 *     }
 *   }
 * }
 *
 * @param json
 * @returns
 */
function transformJsonToFlavors(json) {
    var initialObj = {};
    var tokenTypes = Object.keys(json);
    tokenTypes.forEach(function (tokenType) {
        var tokenTypeValue = json[tokenType];
        var tokenTypeKeys = Object.keys(tokenTypeValue);
        var cloneObj = __assign({}, initialObj);
        initialObj = tokenTypeKeys.reduce(function (oldObj, theme) {
            if (!tokenTypeValue) {
                return oldObj;
            }
            if (!cloneObj[theme]) {
                cloneObj[theme] = {};
            }
            cloneObj[theme][tokenType] = formatFlavorJsonToDesignTokens(tokenTypeValue[theme]);
            return cloneObj;
        }, cloneObj);
    });
    return initialObj;
}
exports.default = transformJsonToFlavors;
/**
 * Function to transform the design tokens to a format the Frontend wants to use it.
 * Which means removing al the nested layers and extra value attribute
 *
 * Example input:
 * "background": {
 *   "background": {
 *     "category": "color",
 *     "exportKey": "color",
 *     "comment": "{gray50}",
 *     "value": "rgba(240, 241, 241, 1)",
 *     "type": "color"
 *   },
 *   "background.surface": {
 *     "category": "color",
 *     "exportKey": "color",
 *     "comment": "{white}",
 *     "value": "rgba(255, 255, 255, 1)",
 *     "type": "color"
 *   }
 * }
 *
 * Example output:
 * {
 *   "background": "rgba(240, 241, 241, 1)",
 *   "backgroundSurface": "rgba(255, 255, 255, 1)"
 * }
 *
 * @param json
 * @param type
 * @param keys
 * @param initialObj
 * @returns
 */
function formatFlavorJsonToDesignTokens(json, keys, initialObj) {
    if (keys === void 0) { keys = []; }
    if (initialObj === void 0) { initialObj = {}; }
    var cloneObj = __assign({}, initialObj);
    return Object.keys(json).reduce(function (oldObj, key) {
        var splitKeys = key.split(".");
        var keyValue = json[key];
        // To have a design token it needs a value and type, in the value is the value of the design token
        // the type contains what type of token it is (color, font).
        //
        // To find the complete token we need to check if the value and type are available within the object
        // of the current key.
        // If they don't we check a nested layer deeper, till they are present.
        // If they are present we group the tokens per type and assign the value to the key it was in.
        if (typeof keyValue === "object" &&
            !("value" in keyValue) &&
            !("type" in keyValue)) {
            cloneObj = formatFlavorJsonToDesignTokens(keyValue, __spreadArray(__spreadArray([], keys, true), splitKeys, true), cloneObj);
        }
        else {
            var value = keyValue.value;
            if (!value) {
                return cloneObj;
            }
            var combinedKeys = __spreadArray(__spreadArray([], keys, true), splitKeys, true);
            if (combinedKeys.length >= 2 && combinedKeys[0] === combinedKeys[1]) {
                combinedKeys.shift();
            }
            var camelCasedKey = camelCaseKeysToString(combinedKeys);
            cloneObj[camelCasedKey] = value;
        }
        return cloneObj;
    }, cloneObj);
}
/**
 * Function that camelCases all keys in the keys array
 *
 * Example input:
 * ['background', 'surface']
 *
 * Example output:
 * "backgroundSurface"
 *
 * @param keys
 * @returns
 */
function camelCaseKeysToString(keys) {
    return keys.reduce(function (oldKey, key, index) {
        if (index === 0) {
            return key;
        }
        var capitalizedFirstLetter = key.charAt(0).toUpperCase() + key.slice(1);
        return oldKey + capitalizedFirstLetter;
    }, "");
}
