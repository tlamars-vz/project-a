console.log(process.env.FIGMA_CLIENT_PAYLOAD_TOKENS);

const FIGMA_PAYLOAD = process.env.FIGMA_CLIENT_PAYLOAD_TOKENS;

if (FIGMA_PAYLOAD) {
  let json;
  try {
    json = JSON.parse(FIGMA_PAYLOAD.toString());
  } catch (err) {
    throw `${FIGMA_PAYLOAD} contains invalid json`;
  }

  console.log(json);
}
