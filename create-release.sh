#!/bin/bash

echo "🚀: Starting release script..."

# Get the current branch
CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)

# Check if user is on master branch other wise give a prompt to continue
if [ "$CURRENT_BRANCH" != "master" ]; then
    # Ask user if they want to continue
    read -n1 -r -p "You are not on the master branch. Do you want to continue? (y/n): " CONTINUE
    echo
    if [ "$CONTINUE" != "y" ]; then
        echo "Exiting..."
        exit 1
    fi

    echo "🚨: Creating new release branch from $CURRENT_BRANCH"
fi

# Create new release branch
echo "💻: Creating new release branch..."
DATE=$(date +%d-%m-%Y)
BRANCH_NAME="release/$DATE"

# Check if branch already exists locally or remotely
EXISTS_LOCAL=$(git branch --list "$BRANCH_NAME")
EXISTS_REMOTE=$(git ls-remote --heads origin "$BRANCH_NAME")

if [ -n "$EXISTS_LOCAL" ] || [ -n "$EXISTS_REMOTE" ]; then
    echo "🚨: Branch $BRANCH_NAME already exists."
    # Add time if branch already exists with date only
    TIME=$(date +%H.%M)
    BRANCH_NAME="${BRANCH_NAME}_${TIME}"
fi

echo "🔼: Creating branch with name $BRANCH_NAME"

# Push new branch to remote
git checkout -b "$BRANCH_NAME"
git push -u origin "$BRANCH_NAME"

# Get the current version from package.json with node
CURRENT_VERSION=$(node -p -e "require('./package.json').version")

# Create git tag with CURRENT_VERSION
echo "🔖: Creating git tag with version $CURRENT_VERSION"
git tag -a "$CURRENT_VERSION" -m "Release $CURRENT_VERSION on $DATE"
git push origin "$CURRENT_VERSION"

echo "🚀: Finished release script..."
